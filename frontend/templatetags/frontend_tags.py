from django import template
from django.conf import settings
from urllib.parse import urlparse

register = template.Library()


@register.simple_tag
def fe_domain():
    url = urlparse(settings.FRONTEND_URL)
    return url.netloc
