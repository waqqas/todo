from django.conf.urls import url
from django.views.generic import TemplateView
from django.urls import path, re_path

from django.contrib.auth.views import PasswordResetConfirmView

urlpatterns = [
    url(r'^reset/(?P<uidb64>[0-9A-Za-z]+)-(?P<token>.+)/$',
        PasswordResetConfirmView.as_view(), name='password_reset_confirm'),
    re_path(
        r'confirm_email/(?P<key>.+)/', TemplateView.as_view(
            template_name="account_email_verification_sent.html"),
        name="account_email_verification_sent",
    ),
]
