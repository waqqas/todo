import React from 'react';
import { Route, Switch } from 'react-router';
import { ConnectedRouter } from 'connected-react-router';
import { Provider } from 'react-redux';
import ConnectedBootstrapProvider from '@bootstrap-styled/redux/lib/ConnectedBootstrapProvider';
import { PersistGate } from 'redux-persist/integration/react';

import store, { history } from './store';
import Tasks from './components/tasks';
import Login from './components/login';
import Register from './components/register';
import ForgotPassword from './components/forgot_password';
import ResetPassword from './components/reset_password';
import GoogleAuthComplete from './components/google_auth_complete';
import ConfirmEmail from './components/confirm_email';
import { Routes } from './routes';
import config from './config';
import { persistor } from './store';

function App() {
  return (
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        <ConnectedRouter history={history}>
          <ConnectedBootstrapProvider>
            <Switch>
              <Route exact path={Routes.home()} component={Tasks} />
              <Route exact path={Routes.login()} component={Login} />
              <Route exact path={Routes.register()} component={Register} />
              <Route exact path={Routes.forgot_password()} component={ForgotPassword} />
              <Route path={Routes.reset_password_confirm()} component={ResetPassword} />
              <Route path={Routes.google_login()} render={() => { window.location.href = `https://accounts.google.com/o/oauth2/v2/auth?response_type=code&client_id=${config.googleClientId}&redirect_uri=${config.serverUrl}${Routes.google_login_callback()}&scope=profile email&access_type=offline&prompt=select_account` }} />
              <Route path={Routes.google_login_callback()} component={GoogleAuthComplete} />
              <Route path={Routes.confirm_email()} component={ConfirmEmail} />
            </Switch>
          </ConnectedBootstrapProvider>
        </ConnectedRouter>
      </PersistGate>
    </Provider>
  );
}

export default App;
