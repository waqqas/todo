import { createStore, applyMiddleware } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import createSagaMiddleware from 'redux-saga';
import rootSaga from './sagas';
import { createBrowserHistory } from 'history';
import { routerMiddleware } from 'connected-react-router';
import createReduxPromiseListener from 'redux-promise-listener';
import { persistStore, persistReducer } from 'redux-persist';
import { persistConfig } from './config';
import createRootReducer from './reducers';

export const history = createBrowserHistory()

const initialState = {};

const sagaMiddleware = createSagaMiddleware();
const reduxPromiseListener = createReduxPromiseListener();

const middlewares = [sagaMiddleware, routerMiddleware(history), reduxPromiseListener.middleware];

const rootReducer = createRootReducer(history);
const persistedReducer = persistReducer(persistConfig, rootReducer)

const store = createStore(
    persistedReducer,
    initialState,
    composeWithDevTools(applyMiddleware(...middlewares))
);

export const promiseListener = reduxPromiseListener;
export const persistor = persistStore(store);

sagaMiddleware.run(rootSaga);

export default store;