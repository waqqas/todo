import _ from 'lodash';
import storage from 'redux-persist/lib/storage';

export const persistConfig = {
    key: 'root',
    whitelist: ['auth'],
    storage,
};

const config = _.pickBy(process.env, (value, key) => {
    return _.startsWith(key, 'REACT_APP_');
});

export default _.mapKeys(config, (value, key) => {
    return _.camelCase(_.replace(key, 'REACT_APP_', ''));
});