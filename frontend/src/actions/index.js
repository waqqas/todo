import { getTasks, getTasksResult } from './tasks';
import { loginByEmail, logout, setToken, resetPasswordByEmail, resetPasswordConfirm, loginByGoogle, registerByEmail, registerByEmailResponse, loginByEmailResponse, resetPasswordByEmailResponse, resetPasswordConfirmResponse, confirmEmail, confirmEmailResponse } from './auth';

export const TaskActions = {
    getTasks,
    getTasksResult,
};

export const AuthActions = {
    loginByEmail,
    loginByEmailResponse,
    logout,
    setToken,
    resetPasswordByEmail,
    resetPasswordByEmailResponse,
    resetPasswordConfirm,
    resetPasswordConfirmResponse,
    loginByGoogle,
    registerByEmail,
    registerByEmailResponse,
    confirmEmail,
    confirmEmailResponse,
}