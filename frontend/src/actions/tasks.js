import { GET_TASKS, GET_TASKS_RESULT } from './types';

export const getTasks = () => ({
    type: GET_TASKS
});
export const getTasksResult = (tasks) => ({
    type: GET_TASKS_RESULT,
    tasks,
});
