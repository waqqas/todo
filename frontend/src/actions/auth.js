import { LOGIN_BY_EMAIL, LOGOUT, SET_TOKEN, RESET_PASSWORD_BY_EMAIL, RESET_PASSWORD_CONFIRM, LOGIN_BY_GOOGLE, REGISTER_BY_EMAIL, REGISTER_BY_EMAIL_RESPONSE, LOGIN_BY_EMAIL_RESPONSE, RESET_PASSWORD_BY_EMAIL_RESPONSE, RESET_PASSWORD_CONFIRM_RESPONSE, CONFIRM_EMAIL, CONFIRM_EMAIL_RESPONSE } from './types';

export const loginByEmail = (payload) => ({
    type: LOGIN_BY_EMAIL,
    payload,
});

export const loginByEmailResponse = (payload) => ({
    type: LOGIN_BY_EMAIL_RESPONSE,
    payload,
});


export const logout = () => ({
    type: LOGOUT,
});

export const setToken = (token) => ({
    type: SET_TOKEN,
    token,
});

export const resetPasswordByEmail = (payload) => ({
    type: RESET_PASSWORD_BY_EMAIL,
    payload,
});

export const resetPasswordByEmailResponse = (payload) => ({
    type: RESET_PASSWORD_BY_EMAIL_RESPONSE,
    payload,
});

export const resetPasswordConfirm = (payload) => ({
    type: RESET_PASSWORD_CONFIRM,
    payload,
});

export const resetPasswordConfirmResponse = (payload) => ({
    type: RESET_PASSWORD_CONFIRM_RESPONSE,
    payload,
});

export const loginByGoogle = (params) => ({
    type: LOGIN_BY_GOOGLE,
    params,
});

export const registerByEmail = (payload) => ({
    type: REGISTER_BY_EMAIL,
    payload,
});

export const registerByEmailResponse = (payload) => ({
    type: REGISTER_BY_EMAIL_RESPONSE,
    payload,
});

export const confirmEmail = (payload) => ({
    type: CONFIRM_EMAIL,
    payload,
});

export const confirmEmailResponse = (payload) => ({
    type: CONFIRM_EMAIL_RESPONSE,
    payload,
});
