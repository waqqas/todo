import apisauce from 'apisauce'
import config from './config'

const create = (baseURL = config.apiServer) => {
    const api = apisauce.create({
        baseURL,
        headers: {
            'Cache-Control': 'no-cache'
        },
        timeout: 10000
    })

    const getTasks = () => api.get('/tasks/');
    const loginByEmail = (params) => api.post('/accounts/login/', params);
    const logout = () => api.post('/accounts/logout/');
    const resetPasswordByEmail = (params) => api.post('/accounts/password/reset/', params);
    const resetPasswordConfirm = (params) => api.post('/accounts/password/reset/confirm/', params);
    const loginByGoogle = ({ code }) => api.post('/accounts/social/google/', { code });
    const registerByEmail = (params) => api.post('/accounts/register/', params);
    const confirmEmail = (params) => api.post('/accounts/register/verify-email/', params);

    return {
        setHeader: api.setHeader,
        getTasks,
        loginByEmail,
        logout,
        resetPasswordByEmail,
        resetPasswordConfirm,
        loginByGoogle,
        registerByEmail,
        confirmEmail,
    }
}

const Api = {
    create
}

export default Api;