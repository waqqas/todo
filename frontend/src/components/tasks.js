import React, { Component, Fragment } from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types';
import { Container } from '@bootstrap-styled/v4';
import { TaskActions } from '../actions';
import Header from './header';

export class Tasks extends Component {
    static propTypes = {
        tasks: PropTypes.array.isRequired,
        getTasks: PropTypes.func.isRequired,
    };

    componentDidMount() {
        this.props.getTasks();
    }

    render() {
        return (
            <Fragment>
                <Header />
                <Container></Container>
            </Fragment>
        )
    };
}

const mapStateToProps = state => ({
    tasks: state.tasks.list,
});

const mapDispatchToProps = (dispatch) => ({
    getTasks: () => dispatch(TaskActions.getTasks()),
});

export default connect(mapStateToProps, mapDispatchToProps)(Tasks);