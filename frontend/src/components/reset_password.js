import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import {
    A,
    Form,
    FormGroup,
    FormText,
    Button,
    Container,
    Row,
} from '@bootstrap-styled/v4';
import { Form as FForm } from 'react-final-form';
import { LinkContainer } from 'react-router-bootstrap';
import { withRouter } from "react-router";
import MakeAsyncFunction from 'react-redux-promise-listener';

import Header from './header'
import { Routes } from '../routes';
import FormField from './form_field';
import { promiseListener } from '../store';
import { RESET_PASSWORD_CONFIRM, RESET_PASSWORD_CONFIRM_RESPONSE } from '../actions/types';

export class ResetPassword extends Component {
    render() {
        const { params: { uid, token } } = this.props.match;

        return (
            <Fragment>
                <Header />
                <Container>
                    <MakeAsyncFunction
                        listener={promiseListener}
                        start={RESET_PASSWORD_CONFIRM}
                        resolve={RESET_PASSWORD_CONFIRM_RESPONSE}
                        reject={RESET_PASSWORD_CONFIRM_RESPONSE}
                    >{onSubmit => (
                        <FForm onSubmit={onSubmit}
                            initialValues={{ uid, token }}>
                            {({ handleSubmit, submitting, pristine, submitError }) => (
                                <Form onSubmit={handleSubmit}>
                                    <FormGroup>
                                        <FormField name="new_password1" type="password" placeholder="New Password" />
                                        <FormField name="new_password2" type="password" placeholder="Confirm Password" />
                                        <FormField name="uid" type="hidden" placeholder="UID" />
                                        <FormField name="token" type="hidden" placeholder="Token" />
                                    </FormGroup>
                                    {submitError &&
                                        (<FormGroup>
                                            {submitError.map((error, i) => <FormText key={i} color="danger">{error}</FormText>)}
                                        </FormGroup>)
                                    }
                                    <Button disabled={submitting || pristine} color="primary" type="submit">Update Password</Button>
                                </Form>
                            )}
                        </FForm>
                    )}</MakeAsyncFunction>
                    <Row>
                        <LinkContainer to={Routes.login()}>
                            <A>Login</A>
                        </LinkContainer>
                    </Row>
                    <Row>
                        <LinkContainer to={Routes.forgot_password()}>
                            <A>Forgot password?</A>
                        </LinkContainer>
                    </Row>
                </Container>
            </Fragment >
        )
    };
}

const mapStateToProps = state => ({
});

const mapDispatchToProps = (dispatch) => ({
});

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(ResetPassword));