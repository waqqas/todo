import React, { Component, Fragment } from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types';
import { Container } from '@bootstrap-styled/v4';
import { withRouter } from "react-router";
import queryString from 'query-string';

import { AuthActions } from '../actions';
import Header from './header';

export class GoogleAuthComplete extends Component {
    static propTypes = {
        loginByGoogle: PropTypes.func.isRequired,
    };

    componentDidMount() {
        let params = queryString.parse(this.props.location.search);
        this.props.loginByGoogle(params);
    }

    render() {
        return (
            <Fragment>
                <Header />
                <Container></Container>
            </Fragment>
        )
    };
}

const mapStateToProps = state => ({
});

const mapDispatchToProps = (dispatch) => ({
    loginByGoogle: (params) => dispatch(AuthActions.loginByGoogle(params)),
});

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(GoogleAuthComplete));