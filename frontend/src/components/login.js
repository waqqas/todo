import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import {
    A,
    Form,
    FormGroup,
    FormText,
    Button,
    Container,
    Row,
} from '@bootstrap-styled/v4';
import { Form as FForm } from 'react-final-form';
import { LinkContainer } from 'react-router-bootstrap';
import MakeAsyncFunction from 'react-redux-promise-listener';

import Header from './header';
import { Routes } from '../routes';
import FormField from './form_field';
import { promiseListener } from '../store';
import { LOGIN_BY_EMAIL, LOGIN_BY_EMAIL_RESPONSE } from '../actions/types';

export class Login extends Component {

    render() {
        return (
            <Fragment>
                <Header />
                <Container>
                    <MakeAsyncFunction
                        listener={promiseListener}
                        start={LOGIN_BY_EMAIL}
                        resolve={LOGIN_BY_EMAIL_RESPONSE}
                        reject={LOGIN_BY_EMAIL_RESPONSE}
                    >{onSubmit => (
                        <FForm onSubmit={onSubmit}>
                            {({ handleSubmit, submitting, pristine, submitError }) => (
                                <Form onSubmit={handleSubmit}>
                                    <FormGroup>
                                        <FormField name="email" type="email" placeholder="Email" />
                                        <FormField name="password" type="password" placeholder="Password" />
                                    </FormGroup>
                                    {submitError &&
                                        (<FormGroup>
                                            {submitError.map((error, i) => <FormText key={i} color="danger">{error}</FormText>)}
                                        </FormGroup>)
                                    }
                                    <Button disabled={submitting || pristine} color="primary" type="submit">Login</Button>
                                </Form>)
                            }
                        </FForm>
                    )}
                    </MakeAsyncFunction>
                    <Row>
                        <LinkContainer to={Routes.forgot_password()}>
                            <A>Forgot password?</A>
                        </LinkContainer>
                    </Row>
                    <Row>
                        <LinkContainer to={Routes.google_login()}>
                            <A>Login via Google</A>
                        </LinkContainer>
                    </Row>
                    <Row>
                        <LinkContainer to={Routes.register()}>
                            <A>Register with email</A>
                        </LinkContainer>
                    </Row>
                </Container>
            </Fragment>
        )
    };
}

const mapStateToProps = state => ({
});

const mapDispatchToProps = (dispatch) => ({
});

export default connect(mapStateToProps, mapDispatchToProps)(Login);