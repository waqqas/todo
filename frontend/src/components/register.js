import React, { Component, Fragment } from 'react'
import { connect } from 'react-redux'

import {
    A,
    Form,
    FormGroup,
    Button,
    Container,
    Row,
    FormText,
} from '@bootstrap-styled/v4';
import { Form as FForm } from 'react-final-form';
import { LinkContainer } from 'react-router-bootstrap';
import MakeAsyncFunction from 'react-redux-promise-listener';


import Header from './header'
import { Routes } from '../routes';
import FormField from './form_field';
import { promiseListener } from '../store';
import { REGISTER_BY_EMAIL, REGISTER_BY_EMAIL_RESPONSE } from '../actions/types';

export class Register extends Component {
    render() {
        return (
            <Fragment>
                <Header />
                <Container>
                    <MakeAsyncFunction
                        listener={promiseListener}
                        start={REGISTER_BY_EMAIL}
                        resolve={REGISTER_BY_EMAIL_RESPONSE}
                        reject={REGISTER_BY_EMAIL_RESPONSE}
                    >{onSubmit => (
                        <FForm onSubmit={onSubmit}>
                            {({ handleSubmit, submitting, pristine, submitError }) => (
                                <Form onSubmit={handleSubmit}>
                                    <FormGroup>
                                        <FormField name="email" type="email" placeholder="Email" />
                                        <FormField name="password1" type="password" placeholder="Password" />
                                        <FormField name="password2" type="password" placeholder="Confirm Password" />
                                    </FormGroup>
                                    {submitError &&
                                        (<FormGroup>
                                            {submitError.map((error, i) => <FormText key={i} color="danger">{error}</FormText>)}
                                        </FormGroup>)
                                    }
                                    <Button disabled={submitting || pristine} color="primary" type="submit">Register</Button>
                                </Form>
                            )}
                        </FForm>
                    )}
                    </MakeAsyncFunction>
                    <Row>
                        <LinkContainer to={Routes.login()}>
                            <A>Login with email</A>
                        </LinkContainer>
                    </Row>
                    <Row>
                        <LinkContainer to={Routes.google_login()}>
                            <A>Login via Google</A>
                        </LinkContainer>
                    </Row>
                </Container>
            </Fragment >
        )
    };
}

const mapStateToProps = state => ({
});

const mapDispatchToProps = (dispatch) => ({
});

export default connect(mapStateToProps, mapDispatchToProps)(Register);