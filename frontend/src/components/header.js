import React, { Component } from 'react';
import { connect } from 'react-redux';
import { push } from 'connected-react-router';
import {
    A,
    Navbar,
    Container,
    NavbarBrand,
    NavbarToggler,
    Collapse,
    Nav,
    NavItem,
    NavLink,
} from '@bootstrap-styled/v4';
import PropTypes from 'prop-types';
// import _ from lodash;
import { LinkContainer } from 'react-router-bootstrap';
import { Routes } from '../routes';
import { AuthActions } from '../actions';


export class Header extends Component {
    static propTypes = {
        push: PropTypes.func.isRequired,
        loggedIn: PropTypes.bool.isRequired,
        logout: PropTypes.func.isRequired,
    };

    state = {
        isOpen: false,
    }

    render() {
        const { state } = this;
        const { loggedIn, logout } = this.props;

        return (
            <Navbar color="faded" light toggleable="lg">
                <Container>
                    <div className="d-flex justify-content-between">
                        <NavbarBrand tag={A} to={Routes.home()}>TODO App</NavbarBrand>
                        <NavbarToggler onClick={() => this.setState({ isOpen: !state.isOpen })} />
                    </div>
                    <Collapse navbar isOpen={state.isOpen}>
                        <Nav navbar className="mr-auto">
                            <NavItem>
                                <LinkContainer to={Routes.home()}>
                                    <NavLink>Home</NavLink>
                                </LinkContainer>
                            </NavItem>
                            {!loggedIn && <NavItem>
                                <LinkContainer to={Routes.login()}>
                                    <NavLink>Login</NavLink>
                                </LinkContainer>
                            </NavItem>}
                            {!loggedIn && <NavItem>
                                <LinkContainer to={Routes.register()}>
                                    <NavLink>Register</NavLink>
                                </LinkContainer>
                            </NavItem>}
                            {loggedIn && <NavItem>
                                <NavLink href="#" onClick={logout}>Logout</NavLink>
                            </NavItem>}
                        </Nav>
                    </Collapse>
                </Container>
            </Navbar >
        )
    };
}

const mapStateToProps = state => ({
    loggedIn: (state.auth.token !== ''),
});

const mapDispatchToProps = (dispatch) => ({
    push: (route) => dispatch(push(route)),
    logout: () => dispatch(AuthActions.logout()),
});

export default connect(mapStateToProps, mapDispatchToProps)(Header);