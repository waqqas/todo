import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import {
    Form,
    FormGroup,
    FormText,
    Button,
    Container,
} from '@bootstrap-styled/v4';
import { Form as FForm } from 'react-final-form';
import { withRouter } from "react-router";
import MakeAsyncFunction from 'react-redux-promise-listener';

import Header from './header'
import FormField from './form_field';
import { promiseListener } from '../store';
import { CONFIRM_EMAIL, CONFIRM_EMAIL_RESPONSE } from '../actions/types';

export class ConfirmEmail extends Component {

    componentDidMount() {
        // submit form
    }
    render() {
        const { params: { key } } = this.props.match;

        return (
            <Fragment>
                <Header />
                <Container>
                    <MakeAsyncFunction
                        listener={promiseListener}
                        start={CONFIRM_EMAIL}
                        resolve={CONFIRM_EMAIL_RESPONSE}
                        reject={CONFIRM_EMAIL_RESPONSE}
                    >{onSubmit => (
                        <FForm onSubmit={onSubmit}
                            initialValues={{ key }}>
                            {({ handleSubmit, submitting, pristine, submitError }) => (
                                <Form onSubmit={handleSubmit}>
                                    <FormGroup>
                                        <FormField name="key" type="hidden" placeholder="Code" />
                                    </FormGroup>
                                    {submitError &&
                                        (<FormGroup>
                                            {submitError.map((error, i) => <FormText key={i} color="danger">{error}</FormText>)}
                                        </FormGroup>)
                                    }
                                    <Button disabled={submitting} color="primary" type="submit">Confirm Email</Button>
                                </Form>
                            )}
                        </FForm>
                    )}</MakeAsyncFunction>
                </Container>
            </Fragment >
        )
    };
}

const mapStateToProps = state => ({
});

const mapDispatchToProps = (dispatch) => ({
});

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(ConfirmEmail));