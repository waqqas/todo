import {
    FormText,
    Input,
} from '@bootstrap-styled/v4';
import { Field } from 'react-final-form';


const FormField = ({ name, type, placeholder }) => (
    <Field name={name}>
        {({ input, meta: { submitError, dirtySinceLastSubmit } }) => {
            const fieldError = submitError && !dirtySinceLastSubmit;
            return (
                <div>
                    <Input type={type} placeholder={placeholder} {...input} />
                    {fieldError ? (
                        <div>
                            {submitError.map((error, i) => <FormText key={i} color="danger">{error}</FormText>)}
                        </div>
                    ) : null}
                </div>
            )
        }}
    </Field>
)

export default FormField;
