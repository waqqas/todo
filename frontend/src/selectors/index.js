import { getToken } from './auth';

export const AuthSelectors = {
    getToken,
}