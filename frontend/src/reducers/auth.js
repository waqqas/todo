import { SET_TOKEN } from '../actions/types';

const initialState = {
    token: '',
}

const tasksReducer = (state = initialState, action) => {
    switch (action.type) {
        case SET_TOKEN:
            return {
                ...state,
                token: action.token,
            };
        default:
            return state;
    }
};

export default tasksReducer;