import { combineReducers } from "redux";
import { connectRouter } from 'connected-react-router';
import themeReducer from '@bootstrap-styled/redux/lib/reducer';

import tasks from './tasks';
import auth from './auth';

const createRootReducer = (history) => combineReducers({
    router: connectRouter(history),
    'bs.redux': themeReducer,
    tasks,
    auth,
});
export default createRootReducer

