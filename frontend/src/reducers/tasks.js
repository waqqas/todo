import {GET_TASKS_RESULT} from '../actions/types';

const initialState = {
    list: []
}

const tasksReducer = (state = initialState, action) => {
    switch(action.type){
        case GET_TASKS_RESULT:
            return {
                ...state,
                list: action.tasks
            };
        default:
            return state;
    }
};

export default tasksReducer;