import { uri } from "url-routes-generator";

export const Routes = {
    home: params => uri("/frontend/")(params),
    login: params => uri("/frontend/login/")(params),
    register: params => uri("/frontend/register/")(params),
    forgot_password: params => uri("/frontend/forgot_password/")(params),
    reset_password_confirm: (uid, token, params) => uri("/frontend/reset/:uid-:token/", { uid, token })(params),
    confirm_email: (key, params) => uri("/frontend/confirm_email/:key/", { key })(params),
    google_login: params => uri("/frontend/google/login/")(params),
    google_login_callback: (params) => uri("/frontend/google/callback/")(params),
};