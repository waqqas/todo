
import { push } from "connected-react-router";
import { call, delay, put, select } from "redux-saga/effects";
import { FORM_ERROR } from 'final-form';
import { AuthActions } from "../actions";
import { Routes } from "../routes";
import { AuthSelectors } from '../selectors';

export function* loginByEmail(api, { payload }) {
    const response = yield call(api.loginByEmail, payload);
    if (response.ok) {
        api.setHeader('Authorization', `Bearer ${response.data.token}`)
        yield put(AuthActions.setToken(response.data.token));
        yield delay(1000);
        yield put(push(Routes.home()));
    }
    else {
        const { non_field_errors } = response.data
        if (non_field_errors) {
            const error = { [FORM_ERROR]: non_field_errors };
            yield put(AuthActions.loginByEmailResponse(error));
        }
        else {
            yield put(AuthActions.loginByEmailResponse(response.data));
        }
    }
}

export function* logout(api) {
    const response = yield call(api.logout);
    if (response.ok) {
        api.setHeader('Authorization', null);
        yield put(AuthActions.setToken(''));
    }
}

export function* resetPasswordByEmail(api, { payload }) {
    const response = yield call(api.resetPasswordByEmail, payload);
    if (response.ok) {
        yield delay(1000);
        yield put(push(Routes.login()));
    }
    else {
        const { non_field_errors } = response.data
        if (non_field_errors) {
            const error = { [FORM_ERROR]: non_field_errors };
            yield put(AuthActions.resetPasswordByEmailResponse(error));
        }
        else {
            yield put(AuthActions.resetPasswordByEmailResponse(response.data));
        }
    }
}

export function* resetPasswordConfirm(api, { payload }) {
    const response = yield call(api.resetPasswordConfirm, payload);
    if (response.ok) {
        yield delay(1000);
        yield put(push(Routes.login()));
    }
    else {
        const { non_field_errors } = response.data
        if (non_field_errors) {
            const error = { [FORM_ERROR]: non_field_errors };
            yield put(AuthActions.resetPasswordConfirmResponse(error));
        }
        else {
            yield put(AuthActions.resetPasswordConfirmResponse(response.data));
        }
    }
}


export function* loginByGoogle(api, { params }) {
    const response = yield call(api.loginByGoogle, params);
    if (response.ok) {
        api.setHeader('Authorization', `Bearer ${response.data.token}`)
        yield put(AuthActions.setToken(response.data.token));
        yield delay(1000);
        yield put(push(Routes.home()));

    }
}

export function* registerByEmail(api, { payload }) {
    const response = yield call(api.registerByEmail, payload);
    if (response.ok) {
        yield delay(1000);
        yield put(push(Routes.login()));
    }
    else {
        const { non_field_errors } = response.data
        if (non_field_errors) {
            const error = { [FORM_ERROR]: non_field_errors };
            yield put(AuthActions.registerByEmailResponse(error));
        }
        else {
            yield put(AuthActions.registerByEmailResponse(response.data));
        }
    }
}

export function* confirmEmail(api, { payload }) {
    const response = yield call(api.confirmEmail, payload);
    if (response.ok) {
        yield delay(1000);
        yield put(push(Routes.login()));
    }
    else {
        const { non_field_errors } = response.data
        if (non_field_errors) {
            const error = { [FORM_ERROR]: non_field_errors };
            yield put(AuthActions.confirmEmailResponse(error));
        }
        else {
            yield put(AuthActions.confirmEmailResponse(response.data));
        }
    }
}


export function* initAuth(api) {
    const token = yield select(AuthSelectors.getToken);
    if (token !== '') {
        api.setHeader('Authorization', `Bearer ${token}`)
    }
}


