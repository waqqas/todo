import { all, takeLatest } from 'redux-saga/effects';
import { getTasks } from './tasks';
import { loginByEmail, loginByGoogle, logout, registerByEmail, resetPasswordByEmail, resetPasswordConfirm, confirmEmail, initAuth } from './auth';
import { CONFIRM_EMAIL, GET_TASKS, LOGIN_BY_EMAIL, LOGIN_BY_GOOGLE, LOGOUT, REGISTER_BY_EMAIL, RESET_PASSWORD_BY_EMAIL, RESET_PASSWORD_CONFIRM } from '../actions/types';
import Api from '../api';
import { REHYDRATE } from 'redux-persist/es/constants';

const api = Api.create();

export default function* root() {
    yield all([
        takeLatest(GET_TASKS, getTasks, api),
        takeLatest(LOGIN_BY_EMAIL, loginByEmail, api),
        takeLatest(LOGOUT, logout, api),
        takeLatest(RESET_PASSWORD_BY_EMAIL, resetPasswordByEmail, api),
        takeLatest(RESET_PASSWORD_CONFIRM, resetPasswordConfirm, api),
        takeLatest(LOGIN_BY_GOOGLE, loginByGoogle, api),
        takeLatest(REGISTER_BY_EMAIL, registerByEmail, api),
        takeLatest(CONFIRM_EMAIL, confirmEmail, api),
        takeLatest(REHYDRATE, initAuth, api),
    ]);
}