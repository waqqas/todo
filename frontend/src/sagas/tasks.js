
import { call, put } from "redux-saga/effects";
import { getTasksResult } from '../actions/tasks';

export function* getTasks(api) {
    const response = yield call(api.getTasks);
    if (response.ok) {
        yield put(getTasksResult(response.data.results));
    }
}
