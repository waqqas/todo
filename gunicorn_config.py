"""gunicorn WSGI server configuration."""
import os
from multiprocessing import cpu_count

# pylint: disable=invalid-name
max_requests = 1000
bind = "0.0.0.0:8000"
workers = cpu_count() * 2 + 1
reload = os.getenv('WEBAPP_RELOAD', False)
