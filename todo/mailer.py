"""Mailer utility to send emails through send_grid_client"""
import logging

from django.conf import settings
from django.template.loader import get_template
from python_http_client import HTTPError
from sendgrid import SendGridAPIClient
from sendgrid.helpers.mail import Mail

log = logging.getLogger(__name__)


class Mailer:  # pylint: disable=too-few-public-methods
    """Send email messages helper class"""

    def __init__(self):
        """set the send grid client"""
        self.mailer = SendGridAPIClient(
            api_key=settings.SENDGRID_API_KEY
        )  # send_grid_client
        self.from_email = settings.EMAIL_SENDER

    def send_email(self, subject: str, template: str, context, to_emails) -> bool:
        """send the message using send_grid_client"""
        message = self.__generate_messages(
            subject=subject, template=template, context=context, to_emails=to_emails
        )

        try:
            self.mailer.send(message)
            return True
        except HTTPError as error:
            log.exception(error)
            return False

    def __generate_messages(self, to_emails, subject, template, context):
        """
        Generate email message using given email template
        :param subject: Email message subject
        :param template: Email template
        :param to_emails: to email address
        :return:
        """
        message_template = get_template(template)
        message = Mail(
            from_email=self.from_email,
            to_emails=to_emails,
            subject=subject,
            html_content=message_template.render(context),
        )

        return message
