"""celery config file"""

import os

from celery import Celery
from celery.schedules import crontab

# set the default Django settings module for the 'celery' program.

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "todo.settings")

app = Celery("todo")

app.config_from_object("django.conf:settings", namespace='')

# Load task modules from all registered Django app configs.
app.autodiscover_tasks()

app.conf.beat_schedule = {
    'task_user_reminder_email': {
        'task': 'tasks.tasks.task_user_reminder_email',
        # 'schedule': crontab(minute='*/1'),
        'schedule': crontab(minute=0, hour=0),
    },
}
