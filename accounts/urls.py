from django.urls import path, include, re_path
from django.conf.urls import url

urlpatterns = [
    path('', include('rest_auth.urls')),
    re_path(r'^register/', include('rest_auth.registration.urls')),
    path('social/', include('accounts.social.urls')),
    path('app/', include('allauth.urls')),
]
