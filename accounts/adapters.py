from allauth.account.adapter import DefaultAccountAdapter
from allauth.socialaccount.adapter import DefaultSocialAccountAdapter
from django.conf import settings
from urllib.parse import urljoin
from django.urls import reverse


def build_frontend_uri(request, location):
    return urljoin(settings.FRONTEND_URL, location)


class AccountAPIAdapter(DefaultAccountAdapter):
    def respond_email_verification_sent(self, request, user):
        pass

    def get_email_confirmation_url(self, request, emailconfirmation):
        url = reverse("account_email_verification_sent",
                      args=[emailconfirmation.key])
        ret = build_frontend_uri(request, url)
        return ret


class SocailAccountAPIAdapter(DefaultSocialAccountAdapter):
    pass
