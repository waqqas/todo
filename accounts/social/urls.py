from django.urls import re_path
from .views import FacebookLogin, FacebookConnect, GoogleLogin, GoogleConnect
from rest_auth.registration.views import (
    SocialAccountListView, SocialAccountDisconnectView
)

urlpatterns = [
    re_path(r'^facebook/$', FacebookLogin.as_view(), name='fb_login'),
    re_path(r'^facebook/connect/$',
            FacebookConnect.as_view(), name='fb_connect'),
    re_path(r'^google/$', GoogleLogin.as_view(), name='google_login'),
    re_path(r'^google/connect/$',
            GoogleConnect.as_view(), name='google_connect'),
    re_path(r'^accounts/$', SocialAccountListView.as_view(),
            name='social_account_list'),
    re_path(r'^accounts/(?P<pk>\d+)/disconnect/$',
            SocialAccountDisconnectView.as_view(), name='social_account_disconnect'),
]
