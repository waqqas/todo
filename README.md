Introduction
---

This is a TODO application that uses django as backend and react.js as a frontend.

URLs (development)
---

- Backend (http://localhost:8000)
- Frontend (http://localhost:3000)

Running (development)
---

- Run `cp .env.dev .env`
- Run `docker compose up`
- Create super user
    - `docker exec -it todo_app_django /bin/bash`
    - `python manage.py createsuperuser --email admin@admin.com`
    - Type in a password
    - `exit`
- Create social application
    - `docker exec -it todo_app_django /bin/bash`
    - `python manage.py set_auth_provider google --name Google 36795089074-dsa7glt377as8si3irl6dvcjoaa64il1.apps.googleusercontent.com mIZf7xPCCv5carQ-3OSmXbYn`
    - `exit`


Configure google social application
---

- Make an app on [GCP console](https://console.cloud.google.com/apis/credentials/oauthclient) and add `OAuth 2.0 Client IDs` to generate a client_id and client_secret
    - Set `http://localhost:3000/frontend/google/callback/` as callback url
- `docker exec -it todo_app_django /bin/bash`
- `python manage.py set_auth_provider google --name Google <client_id> <client_secret>`
