from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response
from .serializers import ReportSerializer
from tasks.models import Task
from django.db.models import F
from django.views.decorators.cache import cache_page
from django.views.decorators.vary import vary_on_headers
from django.core.cache.backends.base import DEFAULT_TIMEOUT
from django.conf import settings

REPORT_CACHE_TTL = getattr(settings, 'REPORT_CACHE_TTL', DEFAULT_TIMEOUT)


@api_view(['GET'])
@cache_page(REPORT_CACHE_TTL)
@vary_on_headers('Authorization')
def stats(request):
    done = Task.objects.filter(done=True, user=request.user).count()
    total = Task.objects.filter(user=request.user).count()

    return Response({"done": done, "total": total})


@api_view(['GET'])
@cache_page(REPORT_CACHE_TTL)
@vary_on_headers('Authorization')
def on_time_completion(request):
    completed = Task.objects.filter(user=request.user,
                                    completion_date__isnull=False, due_date__isnull=False, completion_date__lte=F('due_date')).count()
    return Response({"completed": completed})
