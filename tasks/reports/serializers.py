from django.contrib.auth.models import User
from rest_framework import serializers
from tasks.models import Task


class ReportSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Task
        fields = ['text', 'done', 'attachment']
