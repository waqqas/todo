from django.conf.urls import include
from django.urls import path
from .views import stats, on_time_completion

urlpatterns = [
    path("stats/", stats, name='report-stats'),
    path("on_time_completion/", on_time_completion,
         name='report-on-time-completion'),
]
