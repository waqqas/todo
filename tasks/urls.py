from django.conf.urls import include
from django.urls import path
from rest_framework import routers
from .views import TaskViewSet

router = routers.DefaultRouter()
router.register(r'', TaskViewSet)


urlpatterns = [
    path('', include(router.urls)),
    path("reports/", include("tasks.reports.urls")),
]
