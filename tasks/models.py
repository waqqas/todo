from django.db import models
from django.utils import timezone
from django.contrib.auth import get_user_model


class Task(models.Model):
    text = models.CharField(max_length=100)
    done = models.BooleanField(default=False)
    attachment = models.FileField(blank=True, null=True)
    due_date = models.DateTimeField(blank=True, null=True)
    completion_date = models.DateTimeField(blank=True, null=True)
    user = models.ForeignKey(get_user_model(), on_delete=models.CASCADE)

    class Meta:
        ordering = ['-id']

    def __str__(self):
        return self.text

    def save(self, *args, **kwargs):
        # update completion date when the task is marked as done
        if(self.done == True):
            self.completion_date = timezone.now()

        super(Task, self).save(*args, **kwargs)
