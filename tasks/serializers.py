from rest_framework import serializers
from .models import Task


class TaskSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Task
        fields = ['pk', 'text', 'done', 'attachment',
                  'due_date', 'completion_date']
