
import logging
from celery import shared_task
from django.conf import settings
from .models import Task
import datetime
from django.utils.timezone import make_aware
from django.db.models import Count
from django.contrib.auth import get_user_model
from django.template.loader import get_template
from django.core.mail import send_mail

User = get_user_model()

# Get an instance of a logger
logger = logging.getLogger(__name__)


@shared_task()
def task_user_reminder_email():
    """Sending email of tasks due on current day"""
    url = settings.SITE_URL
    subject = "Tasks due today"
    template = "tasks/email/due_today.html"

    start_of_day = end_of_day = datetime.datetime.utcnow()
    start_of_day = datetime.datetime.combine(
        start_of_day.date(), datetime.time())
    end_of_day = datetime.datetime.combine(end_of_day.date(), datetime.time())
    end_of_day += datetime.timedelta(days=1)
    start_of_day = make_aware(start_of_day)
    end_of_day = make_aware(end_of_day)

    due_tasks = Task.objects.values('user_id').annotate(num_tasks=Count('user_id')).filter(
        due_date__gte=start_of_day, due_date__lt=end_of_day, done=False)

    for task in due_tasks:
        if task['num_tasks'] > 0:
            try:
                user = User.objects.get(pk=task['user_id'])

                logger.info(
                    f"user: {user} has {task['num_tasks']} pending tasks today")
                email_context = {
                    "task": task,
                    "user": user,
                }

                message_template = get_template(template)
                message = message_template.render(email_context)

                send_mail(subject, "", None, [
                          user.email], html_message=message)

            except:
                raise
