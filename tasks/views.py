from rest_framework import viewsets
from .serializers import TaskSerializer
from .models import Task
from rest_framework.response import Response
from rest_framework.decorators import action
import logging

logger = logging.getLogger(__name__)


class TaskViewSet(viewsets.ModelViewSet):
    queryset = Task.objects.all()
    serializer_class = TaskSerializer

    def get_queryset(self):
        return Task.objects.filter(user=self.request.user)

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)

    @action(detail=True, methods=['get'])
    def similar(self, request, pk=None):
        current_task = self.get_object()

        user_tasks = Task.objects.filter(user=request.user)

        similar_tasks = Task.objects.filter(
            user=request.user, text__icontains=current_task.text).exclude(pk=current_task.pk)

        result = list(similar_tasks)
        for task in user_tasks:
            if task.pk != current_task.pk and task.text in current_task.text:
                result.append(task)

        serializer = self.get_serializer(result, many=True)
        return Response(serializer.data)
